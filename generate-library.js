const Chance = require('chance');
const fs = require('fs');
const chance = new Chance();

const authors = Array.from({ length: 10 } , () => {
  return {
    id: chance.guid(),
    name: chance.name(),
    country: chance.country({ full: true })
  }
})

function createBook() {
  const word = chance.capitalize(chance.word({ syllables: 3 }));
  const animal = chance.animal();
  const singleAnimal = animal.endsWith('s') ? animal.slice(0, -1) : animal;
  const name = `${word} the ${singleAnimal}`;
  const synopsis = chance.paragraph({ sentences: 4 });
  const author = chance.pickone(authors).id
  const id = chance.guid()
  return { id, name, author, synopsis };
}

function getBooks(size) {
  return Array.from({ length: size }, () => createBook());
}

const library = {
  books: getBooks(100),
  authors
}
fs.writeFileSync('library.json', JSON.stringify(library, null, 2));
