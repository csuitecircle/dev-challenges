# C-Suit Circle Senior Front-End Challenge

Develop a frontend system to manage a kid's book catalog. Every book has:

- an identifier
- a name
- a description
- an author

Every author:

- id
- name
- country

The app should have 2 pages:

- Index page:
  - list all books with name and thumbnail
  - every book show have a bookmark icon, when clicked the book should be
    added/removed to/from the favourites
  - a sidebar with the names and links to the favourite books
- Article page:
  - Display all the info about the book

Provide some basic styling without using any CSS framework, or preprocessor; you
can (and should) use CSS variables and flexbox.

Please spend around 2-3 hours on the test, not much more.

Feel free to make any assumption you want. Same goes with any big decision you
have to make. We would advise you to write assumptions and decisions down, it
makes great material for interview discussion!

We won't judge your solution "by the looks" but by your code. Having said that,
a careful user experience (even if things don't look fancy) will be considered
very positively. The main aspects we will consider about the code:

- Naming and sticking to a style or convention
- Algorithms and Data structures (using objects, lists, and related
  appropriately; same for algorithms)
- Cleanness and readability of code
- Correct use of tools and libraries, if any

You don't need to write any backend code. You can create a fake data library
with the provided generator:

```sh
node generate-library.js
```
