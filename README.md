# C-Suit Circle Challenges guide

Submit all the answers by creating a **private** GitHub/Gitlab/Bitbucket repository and sharing
it with **csuitecircle**.

Generate fake data with node.js: `node generate-library.js`

- [Senior Front-end developer challenge](./senior-challenge.md)
- [Juior Front-end developer challenge](./junior-challenge.md)

